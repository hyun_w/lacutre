import json


class Post:
    def __init__(self, title=None, content=None):
        self.title = title
        self.content = content

    @staticmethod
    def get_posts():
        with open('../dataset/post.json', 'r') as f:
            data = json.load(f)
        return data

    @staticmethod
    def get_post(id):
        with open('../dataset/post.json', 'r') as f:
            data = json.load(f)

        return [row for row in data if row['id'] == id] or dict()

    def save_post(self):
        posts = self.get_posts()
        next_id = len(posts)

        posts.append(dict(id=next_id, title=self.title, content=self.content))

        with open('../dataset/post.json', 'w') as f:
            json.dump(posts, f, ensure_ascii=False, indent=4)

        return 'success'
