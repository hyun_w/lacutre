
# TUPLE

test_tuple = (1, 2, 3, 4, 5)

print(test_tuple)
print(test_tuple[0])
print(test_tuple[1])

# LIST
l = [1, 2, 3, 4, 5]

l = [1.1, 'test', True, False, 0]

for row in l:
    print(row)


# DICT

d = {'id': 1, 'title': 'test title', 'text': 'This is a test text'}

print(d)
print(d['id'])
print(d['title'])

for key in d:
    print(key)

for value in d.values():
    print(value)

for key, value in d.items():
    print(key, ':', value)

for key, value in d.items():
    print('{}: {}'.format(key, value))


