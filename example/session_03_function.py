import json


def save_post(data, id, title, content):
    with open('../dataset/post.json', 'w') as f:
        data.append(dict(id=id, title=title, content=content))
        json.dump(data, f, ensure_ascii=False, indent=4)


def get_posts():
    with open('../dataset/post.json', 'r') as f:
        data = json.load(f)
    return data


def save_post_2(data):
    with open('../dataset/post.json', 'w') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def insert_post(title, content):
    posts = get_posts()
    next_id = len(posts)

    posts.append(dict(id=next_id, title=title, content=content))
    save_post_2(posts)

    return 'success'


# TODO: UPDATE

if __name__ == '__main__':
    insert_post('테스트1', '테스트1')
    insert_post('테스트2', '테스트2')
    insert_post('테스트3', '테스트3')
    insert_post('테스트4', '테스트4')
