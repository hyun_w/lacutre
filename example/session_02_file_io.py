import json


with open('test.txt', 'w') as f:
    f.write('Say hello')

with open('test.txt', 'r') as f:
    for line in f.readlines():
        print(line)

with open('../dataset/post.json', 'w') as f:
    data = [dict(title='Test title', content='안녕하세요')]
    json.dump(data, f, ensure_ascii=False, indent=4)

with open('../dataset/post.json', 'r') as f:
    data = json.load(f)
    print(data)
